#!/usr/local/bin/python3

import sys
import platform
import subprocess
import colorsys
import logging
from PIL import Image, ImageOps, ImageColor
from lifxlan import LifxLAN, MultiZoneLight

# Installation zone layout
# 9 10 11 12 13 14 15
# 8                16
# 7 6  5  4  3  2  1

logging.basicConfig(level=logging.INFO)

# LED light strip physical installation zone offset
install_offset = 8

class display:
    def __init__(self):
        self.data = self.gather_info()
        [self.width, self.height] = self.resolution()

    def gather_info(self):
        display_data = subprocess.check_output('system_profiler SPDisplaysDataType', shell=True).decode("utf-8")
        return display_data

    def check_internal(self):
        display_type = self.data.split('\n')[14]
        if 'Built-In' in display_type:
            return True
        else:
            return False

    def resolution(self):
        display_resolution = self.data.split('\n')[15]
        width = display_resolution.split(' x ')[0].split(' ')[-1]
        height = display_resolution.split(' x ')[1].split(' ')[0]
        logging.info("Display resolution - %s x %s" %(width, height))
        return [width, height]

class background(display):
    def __init__(self, wallpaper=None):
        super().__init__() 
        if wallpaper is None:
            wallpaper_file = self.get_system_wallpaper_path()
        else:
            wallpaper_file = wallpaper
        self.wallpaper = Image.open(wallpaper_file)

    def get_system_wallpaper_path(self):
        home_dir = subprocess.check_output("echo $HOME", shell=True).decode("utf-8").replace('\n','')
        recent_wallpapers = subprocess.check_output(\
            'sqlite3 %s/Library/Application\ Support/Dock/desktoppicture.db "select * from data;"' \
            %(home_dir), shell=True).decode("utf-8").split('\n')

        for p in range(len(recent_wallpapers) - 1, 0, -1):
            if "/" in recent_wallpapers[p]:
                path = recent_wallpapers[p]
                path = path[0:path.rfind('/')]
                if '~' in path:
                    path = home_dir + path.replace('~','')
                break

        current_wallpaper_path = path + "/" + recent_wallpapers[-2]

        # Hardcoding for Dad demo
        # current_wallpaper_path = "/Users/zcumming/repos/RADiance/test_wallpapers/hl64rn5g7bk01.jpg"

        logging.info("Current wallpaper path - %s" %(current_wallpaper_path))
        return current_wallpaper_path 

    def fit_to_display(self, wallpaper):
        logging.info("Scaling wallpaper %s x %s to display resolution" %(wallpaper.size[0], wallpaper.size[1]))
        self.fitted_wallpaper = ImageOps.fit(wallpaper, (int(self.width), int(self.height)))
        self.fitted_wallpaper.save("Screensaver/screensaver.png")
        return self.fitted_wallpaper

    def remove_crust(self, edge_percentage=5):
        image = self.fit_to_display(self.wallpaper)
        [width,height] = image.size
        self.edge_size = width * edge_percentage // 100
        logging.info("Using crust edge - %s%%, Size - %s px" %(edge_percentage, self.edge_size))
        edge_boxes = []
        # Top edge
        edge_boxes.append((0, 0, width, self.edge_size))
        # Right edge
        edge_boxes.append(((width - self.edge_size), 0, width, height))
        # Bottom edge
        edge_boxes.append((0, (height - self.edge_size), width, height))
        # Left edge
        edge_boxes.append((0, 0, self.edge_size, height))
       
        edges = [] 
        for box in edge_boxes:
            edges.append(image.crop(box))
        return edges
    
    def unroll(self, edges):
        total_width = edges[0].size[0] * 2 + edges[1].size[1] * 2
        total_height = self.edge_size
        logging.debug("Unrolling crust to - %s x %s" %(total_width, total_height))

        unroll = Image.new('RGBA', (total_width, total_height), 0)

        width_pointer = 0
        rotate = 0
        for image in edges:
            [width,height] = image.size
            if width > height: 
                new_width = width
            else: 
                new_width = height
            box = ( width_pointer, 0, width_pointer + new_width, total_height)
            width_pointer += new_width
            rotated_image = image.rotate(90*rotate, expand=True)
            logging.debug("Box: %s - Rotated edge %s Dimenstions %s x %s" %(box, rotate, rotated_image.size[0], rotated_image.size[1]))
            unroll.paste(rotated_image, box)
            rotate += 1

        #if SAVE: unroll.save("wallpaper_unroll.png")
        return unroll
    
    def cut_into_zones(self, edges, num_zones=16):
        unroll = self.unroll(edges)
        [width,height] = unroll.size
        logging.info("Dividing in %s zones" %(num_zones))
        zone_width = width // num_zones
        zones = []
        for zone_index in range(0,num_zones):
            box = (zone_width*zone_index, 0, zone_width*(zone_index+1), height)
            zone = unroll.crop(box)
            zones.append(zone)
        return zones

class zone():
    def __init__(self, image, box=None):
        self.image = image
        [self.width,self.height] = image.size
        self.colors = self.image.getcolors(self.width*self.height)

    def overlay(self, color, name=None):
        overlay = Image.new("RGBA", (self.width,self.height), color)
        blend = Image.blend(self.image, overlay, 0.8)
        if SAVE: blend.save("blended_%s.png" %(name))

    def average_color(self):
        total_pixels = self.width*self.height
        red_sum = 0
        green_sum = 0
        blue_sum = 0
        for count, rgb in self.colors: 
            if rgb[0] == 0 and rgb[1] == 0 and rgb[2] == 0 and rgb[3] == 0:
                continue
            red_sum += count * rgb[0]
            green_sum += count * rgb[1]
            blue_sum += count * rgb[2]

        red_avg = red_sum // total_pixels
        green_avg = green_sum // total_pixels
        blue_avg = blue_sum // total_pixels
        hsv = self.convert_to_hsv((red_avg, green_avg, blue_avg))
        logging.info("Zone average color: %s %s %s" %(hsv[0], hsv[1], hsv[2]))
        return hsv

    def common_color(self):
        max_count = 0
        common_rgb = []
        for count, rgb in self.colors:
            if rgb[0] == 0 and rgb[1] == 0 and rgb[2] == 0 and rgb[3] == 0:
                continue
            if count > max_count:
                max_count = count 
                common_rgb = rgb
        hsv = self.convert_to_hsv((common_rgb[0], common_rgb[1], common_rgb[2]))
        logging.debug("Zone most common color: %s %s %s" %(hsv[0], hsv[1], hsv[2]))
        return hsv

    def convert_to_hsv(self, rgb):
        hsv = colorsys.rgb_to_hsv(rgb[0], rgb[1], rgb[2])
        return hsv

class rad_lifx():
    def __init__(self, num_lights=None):
        try:
            fp = open("lifxz.net", "r")
            net = fp.readlines()
            mac = net[0].strip('\n')
            ip = net[1]
            logging.info("Found saved Lifx MAC and IP - %s, %s" %(mac, ip))
            self.strip = MultiZoneLight(mac, ip)
        except:
            self.strip = self.discover(num_lights)
            pass

    def discover(self, num_lights):
        logging.info("Discovering available LIFX lights...")
        multizone_lights = LifxLAN(num_lights, False).get_multizone_lights()
        if len(multizone_lights) > 0:
            lifxz = multizone_lights[0]
            mac = lifxz.get_mac_addr()
            ip = lifxz.get_ip_addr()
            label = lifxz.get_label()
            mac_fp = open("lifxz.net", "w")
            mac_fp.write(mac)
            mac_fp.write('\n')
            mac_fp.write(ip)
            mac_fp.close()
            logging.info("Found %s - MAC %s, IP - %s" %(label, mac,ip))
            return lifxz
        else:
            logging.info("No multizone lights found")
            return False
    
    def set_zone_color(self, zone_number, color):
        zone_number = zone_number + install_offset
        if zone_number > 15:
            zone_number = zone_number - 16
        hsvk = []
        hue = int(color[0] * 65535)
        sat = int(color[1] * 65535)
        val = int(color[2] / 255 * 65535)
        hsvk.append(hue)

        hsvk.append(sat)
        #hsvk.append(65535)

        # Set max value
        #hsvk.append(65535)
        hsvk.append(val)
        
        # Set lowest temperature
        temperature = 6000
        hsvk.append(temperature)

        pretty_print_colors = []
        pretty_print_colors.append(int(color[0] * 360))
        pretty_print_colors.append(int(color[1] * 100))
        pretty_print_colors.append(int(color[2] / 255 * 100))
        pretty_print_colors.append(temperature)

        logging.info("Setting zone %s color to %s" %(zone_number, pretty_print_colors))
        ret = self.strip.set_zone_color(zone_number, zone_number + 1, hsvk, 1000, True, 1)

    def get_zone_colors(self):
        print(self.strip.get_color_zones())

def main():
    bg = background()
    if bg.check_internal():
        logging.info("Internal display detected, exiting")
        sys.exit()

    edges = bg.remove_crust(40)    
    zones = bg.cut_into_zones(edges, 16)

    strip = rad_lifx()

    for image in zones:
        zone_number = zones.index(image)
        chunk = zone(image)
        color = chunk.common_color()
        strip.set_zone_color(zone_number, color)
# Main
if __name__ == "__main__":
    main()
